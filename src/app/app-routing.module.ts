import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const customerModule = () => import('./customer/customer.module').then(x => x.CustomerModule);

const routes: Routes = [
  { path: '', redirectTo: 'customer/list', pathMatch: 'full' },
  { path: 'customer', loadChildren: customerModule },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
