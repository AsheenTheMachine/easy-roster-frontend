﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { CustomerService, AlertService } from '../_services';
import { Customer } from '../_models';

@Component({ templateUrl: 'list.component.html' })
export class CustomerListComponent implements OnInit {
  customers!: Customer[];

  constructor(
    private customerService: CustomerService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    //fetch all customers and populate grid
    this.customerService.getAll()
      .pipe(first())
      .subscribe({
        next: data => {
          this.customers = data;
          $(function () {
            $("#dtCustomers").DataTable(
              {
                "order": [[1, "asc"]]
              }
            );
          });
        }
    });
  }

  //delete customer by id
  deleteCustomer(id: number) {
    const customer = this.customers.find(x => x.id === id);
    if (!customer) return;
    this.customerService.delete(id)
      .pipe(first())
      .subscribe(() => {
        this.customers = this.customers.filter(x => x.id !== id);
        this.alertService.success('Customer deleted');
      });
  }
}