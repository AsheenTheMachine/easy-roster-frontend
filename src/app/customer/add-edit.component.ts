﻿import { Component, OnInit, Type } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { CustomerService, AlertService } from '../_services';
import { Customer } from '../_models';

@Component({ templateUrl: 'add-edit.component.html' })
export class AddEditComponent implements OnInit {
    form!: FormGroup;
    id!: number;
    isAddMode!: boolean;
    loading = false;
    submitted = false;
    customer: Customer;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private customerService: CustomerService,
        private alertService: AlertService
    ) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.form = this.formBuilder.group({
            firstName:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50)
                    ]
                ],
            surname:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50)
                    ]
                ],
            email:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(3),
                        Validators.maxLength(50),
                        Validators.email
                    ]
                ],
            cellphone:
                [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(10),
                        Validators.maxLength(13)
                    ]
                ],
            invoiceTotal:
                [
                    '',
                    [

                    ]
                ],
            type:
                [
                    '',
                    [
                        Validators.required,
                        Validators.min(1)
                    ]
                ]
        });

        if (!this.isAddMode) {
            this.customerService.get(this.id)
                .pipe(first())
                .subscribe({
                    next: data => {
                        this.form.patchValue(data)
                        this.f['type'].setValue(data.type);
                    }
                });
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        if (this.isAddMode) {
            this.create();
        } else {
            this.update();
        }
    }

    //create new customer
    private create() {
        this.customer = this.form.value;
        this.customer.type = this.f['type'].value;

        this.customerService.create(this.customer)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Customer added!', { keepAfterRouteChange: true });
                    this.router.navigate(['../../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                }
            })
            .add(() => this.loading = false);
    }

    //update customer
    private update() {
        this.customer = this.form.value;
        this.customer.type = this.f['type'].value;

        this.customerService.update(this.id, this.customer)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Customer updated!', { keepAfterRouteChange: true });
                    this.router.navigate(['../../list'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error, { keepAfterRouteChange: true });
                }
            })
            .add(() => this.loading = false);
    }
}