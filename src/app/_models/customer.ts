export class Customer {
  id: number;
  firstName: string;
  surname: string;
  email: string;
  cellphone: string;
  invoiceTotal: number;
  type: Number;
}
