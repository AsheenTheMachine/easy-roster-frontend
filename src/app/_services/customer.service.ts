import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Customer } from '../_models/customer';
import { environment } from '../../environments/environment';

const baseUrl = environment.apiUrl + 'customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  create(params: any) {
    return this.http.post(`${baseUrl}/new`, params);
  }

  update(id: number, params: any): Observable<any> {
    return this.http.put(`${baseUrl}/edit/${id}`, params)
      .pipe(map(x => {
        return x;
      }));
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/remove/${id}`)
            .pipe(map(x => {
                return x;
            }));
  }

  get(id: number): Observable<Customer> {
    return this.http.get<Customer>(`${baseUrl}/${id}`);
  }

  getAll(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${baseUrl}/list/`);
  }
}
