# README #

# About
Angular 13 Frontend application for managing customers

# Features

1. Angular 13
2. Bootstrap 5
3. Connects to .Net Core 6.0 API
4. Form Validation
5. Reactive Forms
6. Manage Customers (CRUD)
7. Angular DataTables with sorting, paging and searching

# How to run the project

Before executing the following steps. Make sure you have the Database and .Net Core API running. API and database can be found here > https://bitbucket.org/AsheenTheMachine/easy-roster-backend/

1. Clone te project to your local machine
2. Open the project in Visual Studio Code
3. Open a new Terminal
4. Type `npm install` to install all node module dependencies
4. Type `ng serve` to host the application
5. After the project has compiled, open your browser and browse `http://localhost:4200/`

Note: Your API must be running on the following Url: `http://localhost:4000` To Change your API Url, update the apiUrl in `environments/environments.ts`


